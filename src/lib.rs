#![doc = include_str!("../README.md")]
pub mod ops;

mod vec3;
pub use vec3::Vec3;

mod impls;
