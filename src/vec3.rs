#[repr(C)]
#[derive(Debug, Clone, PartialEq)]
pub struct Vec3<T> {
    pub x: T,
    pub y: T,
    pub z: T
}

impl <T> Vec3 <T> {
    /// ```
    /// use gfxmath_vec3::Vec3;
    /// 
    /// let v = Vec3::<f32>::new(2.5, 0.0, -19.5);
    /// 
    /// assert_eq!(2.5, v.x);
    /// assert_eq!(0.0, v.y);
    /// assert_eq!(-19.5, v.z);
    /// ```
    pub fn new(x: T, y: T, z: T) -> Self {
        return Self { x, y, z }
    }

    pub fn as_ptr(&self) -> *const T {
        self as *const _ as *const T
    }

    pub fn as_mut_ptr(&mut self) -> *mut T {
        self as *mut _ as *mut T
    }

    ///
    /// ```
    /// use gfxmath_vec3::Vec3;
    /// 
    /// let a = Vec3::<f32>::new(1.0, 2.0, 3.0);
    /// 
    /// let a_slice = a.as_slice();
    /// 
    /// assert_eq!(1.0, a_slice[0]);
    /// assert_eq!(2.0, a_slice[1]);
    /// assert_eq!(3.0, a_slice[2]);
    /// ```
    pub fn as_slice(&self) -> &[T] {
        unsafe { std::slice::from_raw_parts(self.as_ptr(), 3) }
    }

    ///
    /// ```
    /// use gfxmath_vec3::Vec3;
    /// 
    /// let mut a = Vec3::<f32>::new(1.0, 2.0, 3.0);
    /// 
    /// {
    ///     let a_slice = a.as_mut_slice();
    /// 
    ///     assert_eq!(1.0, a_slice[0]);
    ///     assert_eq!(2.0, a_slice[1]);
    ///     assert_eq!(3.0, a_slice[2]);
    /// 
    ///     a_slice[2] = 108.0;
    ///     assert_eq!(108.0, a_slice[2]); 
    /// }
    /// 
    /// a.x = 4.5;
    /// assert_eq!(4.5, a.x);
    /// 
    /// let a_slice = a.as_mut_slice();
    /// 
    /// assert_eq!(4.5, a_slice[0]);
    /// assert_eq!(2.0, a_slice[1]);
    /// assert_eq!(108.0, a_slice[2]);
    /// ```
    pub fn as_mut_slice(&mut self) -> &mut [T] {
        unsafe { std::slice::from_raw_parts_mut(self.as_mut_ptr(), 3) }
    }
}

impl <T> Vec3<T> where T: Clone {
    /// ```
    /// use gfxmath_vec3::Vec3;
    /// 
    /// let v = Vec3::<f32>::all(2.5);
    /// 
    /// assert_eq!(2.5, v.x);
    /// assert_eq!(2.5, v.y);
    /// assert_eq!(2.5, v.z);
    /// ```
    pub fn all(val: T) -> Self {
        Self::new(val.clone(), val.clone(), val.clone())
    }
}

/// ```
/// use gfxmath_vec3::{Vec3, vec3};
/// 
/// let v = vec3!(1.0, 2.0, 3.0);
/// 
/// assert_eq!(1.0, v.x);
/// assert_eq!(2.0, v.y);
/// assert_eq!(3.0, v.z);
/// ```
#[macro_export]
macro_rules! vec3 {
    ($x: expr, $y: expr, $z: expr) => {
        {
            Vec3::new($x, $y, $z)
        }
    }
}
