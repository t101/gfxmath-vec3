use crate::Vec3;
use core::ops::Add;


///
/// ```
/// use gfxmath_vec3::Vec3;
/// let a = Vec3::new(1.0, 2.0, 3.0);
/// let b = Vec3::new(3.0, 4.0, 5.0);
/// 
/// let res: Vec3<f32> = &a + &b;
/// 
/// assert_eq!(1.0, a.x);
/// assert_eq!(2.0, a.y);
/// assert_eq!(3.0, a.z);
/// 
/// assert_eq!(3.0, b.x);
/// assert_eq!(4.0, b.y);
/// assert_eq!(5.0, b.z);
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(6.0, res.y);
/// assert_eq!(8.0, res.z);
/// ```
#[opimps::impl_ops(Add)]
#[inline]
fn add<T>(self: Vec3<T>, rhs: Vec3<T>) -> Vec3<T> where T: Add<Output = T> + Copy {
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec3::new(
        l[0] + r[0],
        l[1] + r[1],
        l[2] + r[2],
    )
}


/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<f32>::new(1.0, 2.0, 3.0);
///
/// let res: Vec3<f32> = (v + 3.0).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// assert_eq!(6.0, res.z);
/// ```
#[opimps::impl_ops_rprim(Add)]
#[inline]
fn add<T>(self: Vec3<T>, rhs: T) -> Vec3<T> where T: Add<Output = T> + Copy {
    let l = self.as_slice();
    Vec3::new(l[0] + rhs, l[1] + rhs, l[2] + rhs)
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<f32>::new(1.0, 2.0, 3.0);
///
/// let res: Vec3<f32> = (3.0 + v).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// assert_eq!(6.0, res.z);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: f32, rhs: Vec3<f32>) -> Vec3<f32> {
    let r = rhs.as_slice();
    Vec3::new(self + r[0], self + r[1], self + r[2])
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<f64>::new(1.0, 2.0, 3.0);
///
/// let res: Vec3<f64> = (3.0 + v).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// assert_eq!(6.0, res.z);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: f64, rhs: Vec3<f64>) -> Vec3<f64> {
    let r = rhs.as_slice();
    Vec3::new(self + r[0], self + r[1], self + r[2])
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<i32>::new(1, 2, 3);
///
/// let res = 3 + v;
/// 
/// assert_eq!(4, res.x);
/// assert_eq!(5, res.y);
/// assert_eq!(6, res.z);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: i32, rhs: Vec3<i32>) -> Vec3<i32> {
    let r = rhs.as_slice();
    Vec3::new(self + r[0], self + r[1], self + r[2])
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<i64>::new(1, 2, 3);
///
/// let res = 3 + v;
/// 
/// assert_eq!(4, res.x);
/// assert_eq!(5, res.y);
/// assert_eq!(6, res.z);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: i64, rhs: Vec3<i64>) -> Vec3<i64> {
    let r = rhs.as_slice();
    Vec3::new(self + r[0], self + r[1], self + r[2])
}
