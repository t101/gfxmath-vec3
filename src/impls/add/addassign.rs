use crate::Vec3;
use core::ops::AddAssign;

///
/// ```
/// use gfxmath_vec3::Vec3;
/// let mut a = Vec3::new(1.0, 2.0, 3.0);
/// let b = Vec3::new(3.0, 4.0, 5.0);
/// 
/// a += b;
/// 
/// assert_eq!(4.0, a.x);
/// assert_eq!(6.0, a.y);
/// assert_eq!(8.0, a.z);
/// ```
#[opimps::impl_ops_assign(AddAssign)]
#[inline]
fn add_assign<T>(self: Vec3<T>, rhs: Vec3<T>) where T: AddAssign<T> + Copy {
    let l = self.as_mut_slice();
    let r = rhs.as_slice();
    
    l[0] += r[0];
    l[1] += r[1];
    l[2] += r[2];
}

///
/// ```
/// use gfxmath_vec3::Vec3;
/// let mut a = Vec3::new(1.0, 2.0, 3.0);
/// a += 4.0;
/// 
/// assert_eq!(5.0, a.x);
/// assert_eq!(6.0, a.y);
/// assert_eq!(7.0, a.z);
/// ```
#[opimps::impl_op_assign(AddAssign)]
#[inline]
fn add_assign<T>(self: Vec3<T>, rhs: T) where T: AddAssign<T> + Copy {
    let l = self.as_mut_slice();
    
    l[0] += rhs;
    l[1] += rhs;
    l[2] += rhs;
}
