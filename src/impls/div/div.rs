use crate::Vec3;
use core::ops::Div;

/// Scalar division with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let b = Vec3::from(a / 2.0);
/// 
/// assert_eq!( 0.25, b.x);
/// assert_eq!( 1.25, b.y);
/// assert_eq!(-1.25, b.z);
/// ```
#[opimps::impl_ops_rprim(Div)]
#[inline]
fn div<T>(self: Vec3<T>, rhs: T) -> Vec3<T> where T: Div<Output = T> + Copy {
    let l = self.as_slice();
    return Vec3::new(
        l[0] / rhs,
        l[1] / rhs,
        l[2] / rhs
    )
}
