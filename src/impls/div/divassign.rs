use crate::Vec3;
use core::ops::DivAssign;

/// Scalar division with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let mut a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let b = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// a /= b;
/// 
/// assert_eq!(1.0, a.x);
/// assert_eq!(1.0, a.y);
/// assert_eq!(1.0, a.z);
/// ```
#[opimps::impl_ops_assign(DivAssign)]
#[inline]
fn div_assign<T>(self: Vec3<T>, rhs: Vec3<T>) where T: DivAssign<T> + Copy {
    let l = self.as_mut_slice();
    let r = rhs.as_slice();

    l[0] /= r[0];
    l[1] /= r[1];
    l[2] /= r[2];
}

impl <T> DivAssign<T> for Vec3<T> where T: DivAssign<T> + Copy {
    /// Scalar division with vector
    /// 
    /// ```
    /// use gfxmath_vec3::Vec3;
    /// 
    /// let mut a = Vec3::<f32>::new(0.5, 2.5, -2.5);
    /// a /= 2.0;
    /// 
    /// assert_eq!( 0.25, a.x);
    /// assert_eq!( 1.25, a.y);
    /// assert_eq!(-1.25, a.z);
    /// ```
    #[inline]
    fn div_assign(&mut self, rhs: T) {
        let l = self.as_mut_slice();
        l[0] /= rhs;
        l[1] /= rhs;
        l[2] /= rhs;
    }    
}
