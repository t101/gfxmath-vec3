use crate::Vec3;

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v: Vec3<f32> = (3.0, 4.0, 9.0).into();
/// 
/// assert_eq!(3.0, v.x);
/// assert_eq!(4.0, v.y);
/// assert_eq!(9.0, v.z);
/// ```
impl <T> From<(T, T, T)> for Vec3 <T> {
    fn from(val: (T, T, T)) -> Self {
        Vec3::new(val.0, val.1, val.2)
    }
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let t = (3.0, 4.0, 9.0);
/// let v: Vec3<f32> = (&t).into();
/// 
/// assert_eq!(3.0, v.x);
/// assert_eq!(4.0, v.y);
/// assert_eq!(9.0, v.z);
/// ```
impl <T> From<&(T, T, T)> for Vec3 <T> where T: Copy {
    fn from(val: &(T, T, T)) -> Self {
        Vec3::new(val.0, val.1, val.2)
    }
}
