use core::ops::{Add, Sub, Mul};
use crate::Vec3;
use crate::ops::Cross;

/// ```
/// use gfxmath_vec3::ops::Cross;
/// use gfxmath_vec3::Vec3;
///
/// let a = Vec3::new(1.0, 3.0, 2.5);
/// let b = Vec3::all(2.0);
///
/// let res = a.cross(b);
/// 
/// assert_eq!(1.0, res.x);
/// assert_eq!(3.0, res.y);
/// assert_eq!(-4.0, res.z);
/// ```
#[opimps::impl_ops(Cross)]
fn cross<T>(self: Vec3<T>, rhs: Vec3<T>) -> Vec3<T>
where T: Add<Output = T> 
    + Mul<Output = T> 
    + Sub<Output = T> 
    + Copy 
{
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec3::new(
        l[1] * r[2] - r[1] * l[2],
        l[2] * r[0] - r[2] * l[0],
        l[0] * r[1] - r[0] * l[1]
    )
}
