use core::ops::Neg;
use crate::Vec3;

/// ```
/// use gfxmath_vec3::ops::Cross;
/// use gfxmath_vec3::Vec3;
///
/// let a = Vec3::new(1.0, 3.0, 2.5);
///
/// let res = -a;
/// 
/// assert_eq!(-1.0, res.x);
/// assert_eq!(-3.0, res.y);
/// assert_eq!(-2.5, res.z);
/// ```
#[opimps::impl_uni_ops(Neg)]
#[inline]
fn neg<T>(self: Vec3<T>) -> Vec3<T>
where T: Neg<Output = T> 
    + Copy 
{
    let l = self.as_slice();

    Vec3::new(
        -l[0],
        -l[1],
        -l[2]
    )
}
