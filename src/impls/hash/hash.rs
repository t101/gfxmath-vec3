use core::hash::Hash;
use crate::Vec3;


impl Hash for Vec3<f32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();

        s.iter().for_each(|i| {
            unsafe { (*(i as *const _ as *const u32)).hash(state) };
        });
    }
}

impl Hash for Vec3<f64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();

        s.iter().for_each(|i| {
            unsafe { (*(i as *const _ as *const u32)).hash(state) };
        });
    }
}

impl Hash for Vec3<i32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec3<i64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec3<u32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec3<u64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}
