use core::ops::{Add, Mul};
use crate::ops::Dot;
use crate::Vec3;

/// ```
/// use gfxmath_vec3::{Vec3, ops::Dot};
/// 
/// let a = Vec3::new(3.0, 4.0, 5.0);
/// let b = Vec3::new(2.0, 1.0, 3.0);
/// 
/// let res = a.dot(b);
/// 
/// assert_eq!(25.0, res);
/// ```
#[opimps::impl_ops(Dot)]
#[inline]
fn dot<T>(self: Vec3<T>, rhs: Vec3<T>) -> T
where T: Add<Output = T>
    + Mul<Output = T>
    + Copy 
{
    let l = self.as_slice();
    let r = rhs.as_slice();

    l[0] * r[0]
    + l[1] * r[1]
    + l[2] * r[2]
}
