use crate::Vec3;
use core::ops::Mul;

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let mut a = Vec3::<f32>::new(1.5, 2.5, -2.0);
/// let b = Vec3::<f32>::new(4.0, 2.0,  4.0);
/// let c = Vec3::from(&a * &b);
/// 
/// a.x = 2.0;
/// 
/// assert_eq!( 6.0, c.x);
/// assert_eq!( 5.0, c.y);
/// assert_eq!(-8.0, c.z);
/// 
/// let c = Vec3::from(a * b);
///
/// assert_eq!( 8.0, c.x);
/// assert_eq!( 5.0, c.y);
/// assert_eq!(-8.0, c.z);
/// ```
#[opimps::impl_ops(Mul)]
#[inline]
fn mul<T>(self: Vec3<T>, rhs: Vec3<T>) -> Vec3<T> where T: Mul<Output = T> + Copy {
    let l = self.as_slice();
    let r = rhs.as_slice();
    Vec3::new(
        l[0] * r[0],
        l[1] * r[1],
        l[2] * r[2],
    )
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let b = Vec3::from(a * 2.0);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// ```
#[opimps::impl_ops_rprim(Mul)]
#[inline]
fn mul<T>(self: Vec3<T>, rhs: T) -> Vec3<T> where T: Mul<Output = T> + Copy {
    return Vec3::new(
        self.x * rhs,
        self.y * rhs,
        self.z * rhs
    );
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let b = Vec3::from(2.0 * a);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: f32, rhs: Vec3<f32>) -> Vec3<f32> {
    return Vec3::new(
        self * rhs.x,
        self * rhs.y,
        self * rhs.z
    );
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<f64>::new(0.5, 2.5, -2.5);
/// let b = Vec3::from(2.0 * a);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: f64, rhs: Vec3<f64>) -> Vec3<f64> {
    return Vec3::new(
        self * rhs.x,
        self * rhs.y,
        self * rhs.z
    );
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<i32>::new(5, 2, -2);
/// let b = Vec3::from(2 * a);
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!( 4, b.y);
/// assert_eq!(-4, b.z);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: i32, rhs: Vec3<i32>) -> Vec3<i32> {
    return Vec3::new(
        self * rhs.x,
        self * rhs.y,
        self * rhs.z
    );
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let a = Vec3::<i64>::new(5, 2, -2);
/// let b = Vec3::from(2 * a);
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!( 4, b.y);
/// assert_eq!(-4, b.z);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: i64, rhs: Vec3<i64>) -> Vec3<i64> {
    return Vec3::new(
        self * rhs.x,
        self * rhs.y,
        self * rhs.z
    );
}
// End of scalar multiplication
