use core::ops::SubAssign;
use crate::Vec3;

/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let mut a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let mut b = Vec3::<f32>::new(1.5, 2.5,  2.0);
/// 
/// a -= &b;
/// 
/// assert_eq!(-1.0, a.x);
/// assert_eq!( 0.0, a.y);
/// assert_eq!(-4.5, a.z);
/// 
/// a -= b;
/// assert_eq!(-2.5, a.x);
/// assert_eq!(-2.5, a.y);
/// assert_eq!(-6.5, a.z);
/// ```
#[opimps::impl_ops_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec3<T>, rhs: Vec3<T>) where T: SubAssign<T> + Copy {
    let l = self.as_mut_slice();
    let r = rhs.as_slice();

    l[0] -= r[0];
    l[1] -= r[1];
    l[2] -= r[2];
}

/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let mut a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// 
/// a -= -1.0;
/// 
/// assert_eq!( 1.5, a.x);
/// assert_eq!( 3.5, a.y);
/// assert_eq!(-1.5, a.z);
/// ```
#[opimps::impl_op_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec3<T>, rhs: T) where T: SubAssign<T> + Copy {
    let l = self.as_mut_slice();

    l[0] -= rhs;
    l[1] -= rhs;
    l[2] -= rhs;
}
