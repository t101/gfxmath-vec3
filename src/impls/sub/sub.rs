use core::ops::Sub;
use crate::Vec3;

/// 
/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let mut a = Vec3::<f32>::new(0.5, 2.5, -2.5);
/// let b = Vec3::<f32>::new(1.5, 2.5,  2.0);
/// let c = Vec3::from(&a - &b);
/// 
/// a.x = 1.0;
/// 
/// assert_eq!(-1.0, c.x);
/// assert_eq!( 0.0, c.y);
/// assert_eq!(-4.5, c.z);
/// 
/// let c2 = Vec3::from(a - b);
///
/// assert_eq!(-0.5, c2.x);
/// assert_eq!( 0.0, c2.y);
/// assert_eq!(-4.5, c2.z);
/// ```
#[opimps::impl_ops(Sub)]
#[inline]
fn sub<T>(self: Vec3<T>, rhs: Vec3<T>) -> Vec3<T> where T: Sub<Output = T> + Copy {
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec3::new(
        l[0] - r[0],
        l[1] - r[1],
        l[2] - r[2]
    )
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::new(1.0, 2.0, 3.0);
///
/// let res = v - 3.0;
/// 
/// assert_eq!(-2.0, res.x);
/// assert_eq!(-1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// ```
#[opimps::impl_ops_rprim(Sub)]
#[inline]
fn sub<T>(self: Vec3<T>, rhs: T) -> Vec3<T> where T: Sub<Output = T> + Copy {
    let l = self.as_slice();
    Vec3::new(l[0] - rhs, l[1] - rhs, l[2] - rhs)
}

/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<f32>::new(1.0, 2.0, 3.0);
/// 
/// let res = 3.0 - v;
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: f32, rhs: Vec3<f32>) -> Vec3<f32> {
    let r = rhs.as_slice();
    Vec3::new(self - r[0], self - r[1], self - r[2])
}


/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<f64>::new(1.0, 2.0, 3.0);
/// 
/// let res = 3.0 - v;
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: f64, rhs: Vec3<f64>) -> Vec3<f64> {
    let r = rhs.as_slice();
    Vec3::new(self - r[0], self - r[1], self - r[2])
}


/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<i32>::new(1, 2, 3);
/// 
/// let res = 3 - v;
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// assert_eq!( 0, res.z);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: i32, rhs: Vec3<i32>) -> Vec3<i32> {
    let r = rhs.as_slice();
    Vec3::new(self - r[0], self - r[1], self - r[2])
}


/// ```
/// use gfxmath_vec3::Vec3;
/// 
/// let v = Vec3::<i64>::new(1, 2, 3);
/// 
/// let res = 3 - v;
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// assert_eq!( 0, res.z);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: i64, rhs: Vec3<i64>) -> Vec3<i64> {
    let r = rhs.as_slice();
    Vec3::new(self - r[0], self - r[1], self - r[2])
}

