#[path = "lib/lib.rs"]
mod lib;
use lib::{init, shaderprogram::ShaderProgram};

use gfxmath_vec3::{Vec3, vec3};
use std::mem::size_of;

use sdl2::{event::Event, keyboard::{Keycode}};

/// Calculates the sinusoid for a given angle, displaying results from
/// -2pi to 2pi.
/// 
/// This function is for demo purposes. It is more efficient to calculate
/// the sinusoid in the shader.
fn get_square() -> Vec<Vec3<f32>> {
    vec!(
        vec3!( 0.5,  0.5, 0.0),
        vec3!( 0.5, -0.5, 0.0),
        vec3!(-0.5, -0.5, 0.0),

        vec3!(-0.5, -0.5, 0.0),
        vec3!(-0.5,  0.5, 0.0),
        vec3!( 0.5,  0.5, 0.0)
    )
}

fn get_ref<'a>(data: &'a Vec<Vec3<f32>>) -> &'a Vec<Vec3<f32>>{
    data
}

fn main() {    
    let (sdl_context, _gl_ctx, window) = init::init("Square");
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut shader = init::get_shader();
    let (vao, vbo) = init::init_buffers();
    let obj = Obj { vao, vbo };

    let default_color = Vec3::new(1.0, 1.0, 0.0);

    shader.add_uniform("color");
    shader.set_uniform_3f("color", &default_color);

    let mut position = vec3!(0.0, 0.0, 0.0);
    let mut scale = vec3!(1.0, 1.0, 1.0);

    shader.add_uniform("position");
    shader.set_uniform_3f("position", &position);

    shader.add_uniform("scale");
    shader.set_uniform_3f("scale", &scale);

    let square = get_square();

    let sq = get_ref(&square);

    'running: loop {
    
        if let Some(_) = process_input(&mut event_pump, &mut position, &mut scale) { break 'running; };
        
        obj.draw(&shader, &||-> &Vec<Vec3<f32>> {
            shader.set_uniform_3f("color", &default_color);

            shader.set_uniform_3f("position", &position);
            shader.set_uniform_3f("scale", &scale);
            sq
        });

        window.gl_swap_window();
        ::std::thread::sleep(::std::time::Duration::from_millis(2));
    }
}

fn process_input(event_pump: &mut sdl2::EventPump, position: &mut Vec3<f32>, scale: &mut Vec3<f32>) -> Option<()> {
    let motion_rate = 0.2;
    for event in event_pump.poll_iter() {
        match event {
            Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                return Some(())
            },

            Event::KeyDown { keycode: Some(Keycode::Up), .. } => {
                position.y += motion_rate;
            }

            Event::KeyDown { keycode: Some(Keycode::Down), .. } => {
                position.y -= motion_rate;

            }

            Event::KeyDown { keycode: Some(Keycode::Left), .. } => {
                position.x -= motion_rate;
            }

            Event::KeyDown { keycode: Some(Keycode::Right), .. } => {
                position.x += motion_rate;
            }

            Event::KeyDown { keycode: Some(Keycode::KpPlus), .. } => {
                scale.x += motion_rate;
                scale.y += motion_rate;
            }

            Event::KeyDown { keycode: Some(Keycode::KpMinus), .. } => {
                scale.x -= motion_rate;
                scale.y -= motion_rate;
            }
            _ => { }
        }
    }
    None
}

trait Drawable<'a, T> {
    fn draw(&self, shader: &ShaderProgram, f: &dyn Fn() -> &'a T);
}

struct Obj {
    vao: u32,
    vbo: u32
}

impl<'a> Drawable<'a, Vec<Vec3<f32>>> for Obj {
    fn draw(&self, shader: &ShaderProgram, f: &dyn Fn() -> &'a Vec<Vec3<f32>>) {
        unsafe {
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
    
            shader.activate();
            gl::BindVertexArray(self.vao);
            
            let data = f();
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(gl::ARRAY_BUFFER, (size_of::<Vec3<f32>>() * data.len()) as gl::types::GLsizeiptr, data.as_ptr() as *const std::ffi::c_void, gl::DYNAMIC_DRAW);

            gl::DrawArrays(gl::TRIANGLES, 0, data.len() as gl::types::GLsizei);
        }
    }
}
